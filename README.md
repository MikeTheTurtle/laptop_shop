# My Laptop Shop Website

A simple exercise to get started on HTML and JavaScript by creating a dummy webshop.

(Fun fact, this is actually my first ever project that I've personally uploaded to Git, following Noroff's tutorial step-by-step, sooo... yay!)
(My promise for the new year is to actually start utilizing Git and commit/push projects frequently so my work isn't lost if lightning strikes. Literally.)

## Installation

Any broswer app of your liking should suffice to access the site locally! No guarantees what will happen if you're using IE though.

## Description

The website offers a simple exercise where you earn currency by pushing a button at some sort of job, whereupon you may deposit these funds to a bank to save them. At this bank you may also borrow currency (with some restrictions) to accumulate funds faster in order to finally buy a laptop of your choice at the store.

## Authors and acknowledgment

Sean Skinner, for being a great teacher and speaker, able to drill HTML and JS fundamentals into my mind where there was no prior knowledge of any of the topics whatsoever.

Erik Gustafsson, for being a great rubber duck and advisor where code syntax is concerned.

My coffee cup, for being my coffee cup.

## Project Status

All done!

Until Sean breaks it.

And probably forces me to fix it.
///// Laptops Doc. Elements /////
const laptopsMenuElement = document.getElementById("laptopsMenu");
const laptopsSpecsElement = document.getElementById("laptopsSpecs");
const laptopsImgElement = document.getElementById("laptopsImg");
const laptopsDescElement = document.getElementById("laptopsDesc");
const laptopsPriceElement = document.getElementById("laptopsPrice");
const buyButtonElement = document.getElementById("buyButton");

///// Variables /////
let laptops = [];

///// URL Fetch /////
fetch("https://hickory-quilled-actress.glitch.me/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToMenu(laptops));

///// Laptop Info Updates /////
const addLaptopsToMenu = (laptops) =>
{
    laptops.forEach(laptop => addLaptop(laptop));
    laptopsSpecsElement.innerHTML = laptops[0].specs.join("<br>");
    laptopsDescElement.innerText = laptops[0].description;
    laptopsPriceElement.innerText = laptops[0].price + " Teeth";
}

const addLaptop = (laptop) =>
{
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsMenuElement.appendChild(laptopElement);
}

const getLaptopImages = async (folder, newImg) =>
{
    //Probably not the best solution but since I know for a fact that there are no more img types that png and jpg, I'll cop out with this
    await fetch(laptopsImgElement.src = "https://hickory-quilled-actress.glitch.me/assets/" + folder + "/" + newImg + ".png")
    .then(async response =>
    {
        if(response.ok)
        {
            return;
        }

        await fetch(laptopsImgElement.src = "https://hickory-quilled-actress.glitch.me/assets/" + folder + "/" + newImg + ".jpg");
    })
    .catch(err =>
    {
        console.error(err);
    })
}

///// Laptop Buttons /////
const handleLaptopsMenuChange = (selection) =>
{
    const selectedLaptop = laptops[selection.target.selectedIndex];
    laptopsSpecsElement.innerHTML = selectedLaptop.specs.join("<br>");
    laptopsDescElement.innerText = selectedLaptop.description;
    laptopsPriceElement.innerText = selectedLaptop.price + " Teeth";
}

const handleBuyButton = () =>
{
    const laptopPrice = laptops[laptopsMenuElement.value - 1];

    if(parseInt(laptopPrice.price) <= accountBalance)
    {
        alert(`Thank you for your purchase! No do-overs!`);
        updateAccountBalance(accountBalance -= laptopPrice.price);
    }
    else if(parseInt(laptopPrice.price) > accountBalance)
    {
        alert(`No.`);
    }
}

///// Event Listeners /////
laptopsMenuElement.addEventListener("change", handleLaptopsMenuChange);
buyButtonElement.addEventListener("click", handleBuyButton);
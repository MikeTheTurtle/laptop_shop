///// Bank Doc. Elements /////
const accountBalanceElement = document.getElementById("accountBalance");
const loanBalanceElement = document.getElementById("loanBalance");
const loanButtonElement = document.getElementById("loanButton");

///// Variables /////
let accountBalance = 0;
let loanBalance = 0;

///// Initialization /////
const initializeAccountBalance = (() =>
{    
    accountBalanceElement.innerText = accountBalance + " Teeth";
}
)()

///// Currency Updates /////
const updateAccountBalance = (teeth) =>
{
    accountBalance = teeth;
    accountBalanceElement.innerText = accountBalance + " Teeth";

    //Making sure that the account never holds negative currency
    if(accountBalance < 0)
    {
        accountBalance = 0;
    }
}

const updateLoanBalance = (teeth) =>
{
    loanBalance = teeth;
    loanBalanceElement.innerText = "Loan Balance: " + loanBalance + " Teeth";

    if (loanBalance === 1)
    {
        loanBalanceElement.innerText = "Loan Balance: " + loanBalance + " Tooth"; //Gotta respect proper English
    }

    //Elements required to be shown or not depending on if loan is active or not
    if(loanBalance <= 0)
    {
        loanBalanceElement.hidden = true;
        payLoanButtonElement.hidden = true;
    }
    else
    {
        loanBalanceElement.hidden = false;
        payLoanButtonElement.hidden = false;
    }
}

///// Bank Buttons /////
const handleLoanButton = () =>
{
    if(loanBalance === 0)
    {
        const requestedLoan = prompt(`Amount requested (DO NOT USE non-digit symbols. Please.): `);

        if(requestedLoan > 0 && requestedLoan <= accountBalance * 2)
        {
            updateAccountBalance(accountBalance += parseInt(requestedLoan));
            updateLoanBalance(parseInt(requestedLoan));

            alert(`Loan approved: ${parseInt(requestedLoan).toFixed(2)}`);
        }
        else if(requestedLoan > 0 && requestedLoan > accountBalance * 2)
        {
            alert(`Loan denied: You're too poor to become rich.`);
        }
        else if(requestedLoan < 0)
        {
            alert(`Error -418: You're a teapot.`);
        }
        else
        {
            alert(`Thank you for wasting our time!`);
        }
    }
    else
    {
        alert(`Do YOU magically grow teeth? ...Actually, don't answer that.`);
    }
}

///// Event Listeners /////
loanButtonElement.addEventListener("click", handleLoanButton);
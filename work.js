///// Work Doc. Elements /////
const salaryBalanceElement = document.getElementById("salaryBalance");
const workButtonElement = document.getElementById("workButton");
const depositButtonElement = document.getElementById("depositButton");
const payLoanButtonElement = document.getElementById("payLoanButton");

///// Variables /////
let salaryBalance = 0;

///// Initialization /////
const initializeSalaryBalance = (() =>
{
    salaryBalanceElement.innerText = salaryBalance + " Teeth";
})()

const hidePayLoanButton = (() =>
{
    payLoanButtonElement.hidden = true; //Shall only show if there's an active loan at the bank
})()

///// Currency Updates /////
const updateSalaryBalance = (teeth) =>
{
    salaryBalance = teeth;
    salaryBalanceElement.innerText = salaryBalance + " Teeth";

    //Making sure that salary never goes negative
    if(salaryBalance < 0)
    {
        salaryBalance = 0;
    }
}

///// Work Buttons /////
const handleWorkButton = () =>
{
    updateSalaryBalance(salaryBalance += 100);
}

const handleDepositButton = () =>
{
    if(loanBalance === 0)
    {
        updateAccountBalance(accountBalance += salaryBalance);

        salaryBalance = 0;
        updateSalaryBalance(salaryBalance);
    }
    else if(loanBalance > 0)
    {
        let loanShare = salaryBalance * 0.1;
        let accountShare = salaryBalance - loanShare;
        
        updateLoanBalance(loanBalance -= loanShare);

        //Making sure that overpayment doesn't get lost by converting the negative loan to positive and adding it to the account share
        if(loanBalance < 0)
        {
            accountShare += loanBalance * -1;
            loanBalance = 0;
        }

        updateAccountBalance(accountBalance += accountShare);

        salaryBalance = 0;
        updateSalaryBalance(salaryBalance);
    }
}

const handlePayLoanButton = () =>
{
    if(salaryBalance <= loanBalance)
    {
        updateLoanBalance(loanBalance -= salaryBalance);

        salaryBalance = 0;
        updateSalaryBalance(salaryBalance);
    }
    else if(salaryBalance > loanBalance)
    {
        let remainingSalary = salaryBalance - loanBalance;

        updateAccountBalance(accountBalance += remainingSalary);
        updateLoanBalance(loanBalance -= salaryBalance);
        loanBalance = 0; //Making sure the loan balance doesn't go negative if we overpay
        
        salaryBalance = 0;
        updateSalaryBalance(salaryBalance);
    }
}

///// Event Listeners /////
workButtonElement.addEventListener("click", handleWorkButton);
depositButtonElement.addEventListener("click", handleDepositButton);
payLoanButtonElement.addEventListener("click", handlePayLoanButton);